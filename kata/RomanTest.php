<?php

namespace Kata;

require dirname(__DIR__).DIRECTORY_SEPARATOR."vendor". DIRECTORY_SEPARATOR. "autoload.php";
use PHPUnit\Framework\TestCase;
use Kata\Roman as Roman;

class RomanTest extends TestCase
{
       public function testTodoTrabaja(){
           $this->assertTrue(true);
       }

       public function testConvertirSimpleDigitoRomano(){
           $this->assertEquals(1, Roman::convert("I"));
           $this->assertEquals(5, Roman::convert("V"), "el resultado debe ser 5");
           $this->assertEquals(10 , Roman::convert("X"));
           $this->assertEquals(50 , Roman::convert("L"));
           $this->assertEquals(100 , Roman::convert("C"));
           $this->assertEquals(500 , Roman::convert("D"));
           $this->assertEquals(1000 , Roman::convert("M"));
       }


}
